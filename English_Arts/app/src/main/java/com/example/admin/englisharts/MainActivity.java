package com.example.admin.englisharts;

import android.content.Intent;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private MediaPlayer ring;
    private ImageView start, logo;
    private TextView account;
    Animation animation, animation_small;
    double screen_width, screen_height;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ring = MediaPlayer.create(MainActivity.this, R.raw.opening);
        ring.setLooping(true);
        ring.start();
        start = (ImageView) findViewById(R.id.start);
        logo = (ImageView) findViewById(R.id.logo);
        account = (TextView) findViewById(R.id.account);
        animation = AnimationUtils.loadAnimation(this.getApplicationContext(), R.anim.myanimationsmall);
        animation_small = AnimationUtils.loadAnimation(this.getApplicationContext(), R.anim.myanimationsmall);

        double dens = Resources.getSystem().getDisplayMetrics().density;
        screen_width = Resources.getSystem().getDisplayMetrics().widthPixels / dens;
        screen_height = Resources.getSystem().getDisplayMetrics().heightPixels / dens;

        scaleScreen(start);
        scaleScreen(logo);
        scaleScreen(account);

        int text_size = (int) (10 * screen_width / 592);
        account.setTextSize(text_size);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ring.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        ring.start();
    }

    public void scaleScreen (View view){
        ConstraintLayout.LayoutParams layoutParams = new ConstraintLayout.LayoutParams(view.getLayoutParams());
        int left = (int) (((ConstraintLayout.LayoutParams) view.getLayoutParams()).leftMargin * screen_width/592);
        int top = (int) (((ConstraintLayout.LayoutParams) view.getLayoutParams()).topMargin * screen_height/ 360);
        layoutParams.height = (int) (view.getLayoutParams().height * screen_height/360);
        layoutParams.width = (int) (view.getLayoutParams().width * screen_width/592);
        view.setLayoutParams(layoutParams);
        view.setX(left);
        view.setY(top);
    }

    public void clickOnStart(View view){
        Intent myIntent = new Intent(MainActivity.this, Level.class);
        startActivity(myIntent);
        start.startAnimation(animation);
    }

    public void clickOnAccount(View view){
        Intent myIntent = new Intent(MainActivity.this, ChangePassword.class);
        startActivity(myIntent);
        start.startAnimation(animation_small);
    }
}
