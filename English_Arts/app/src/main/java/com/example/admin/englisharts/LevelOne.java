package com.example.admin.englisharts;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.PowerManager;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class LevelOne extends AppCompatActivity {
    SoundManager sound;

    private MediaPlayer ring, correct, incorrect, song;
    private TextView vocab;
    private ImageView play,manual, review, back, home, board;
    private ImageView father, mother, grandmother, grandfather, brother, sister, family, bee_one, bee_two, bee_three, win, lose, close;
    private boolean isReview = false;
    int count = 7;
    private int order;
    private int live = 3;
    String BASE = "1234567";
    StringBuilder remain = new StringBuilder(BASE);
    boolean screenOn, isPlaying;
    Animation animation, animation_small;
    double screen_width, screen_height;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level_one);
        vocab = (TextView) findViewById(R.id.vocab);
        play = (ImageView) findViewById(R.id.play);
        manual = (ImageView) findViewById(R.id.manual);
        review = (ImageView) findViewById(R.id.review);
        father = (ImageView) findViewById(R.id.father);
        mother = (ImageView) findViewById(R.id.mother);
        grandmother = (ImageView) findViewById(R.id.grandmother);
        grandfather = (ImageView) findViewById(R.id.grandfather);
        brother = (ImageView) findViewById(R.id.brother);
        sister = (ImageView) findViewById(R.id.sister);
        family = (ImageView) findViewById(R.id.family);
        bee_one = (ImageView) findViewById(R.id.bee_one);
        bee_two = (ImageView) findViewById(R.id.bee_two);
        bee_three = (ImageView) findViewById(R.id.bee_three);
        win = (ImageView) findViewById(R.id.win);
        lose = (ImageView) findViewById(R.id.lose);
        close = (ImageView) findViewById(R.id.close);
        back = (ImageView) findViewById(R.id.back);
        home = (ImageView) findViewById(R.id.home);
        board = (ImageView) findViewById(R.id.board);
        animation = AnimationUtils.loadAnimation(this.getApplicationContext(), R.anim.myanimation);
        animation_small = AnimationUtils.loadAnimation(this.getApplicationContext(), R.anim.myanimationsmall);

        sound = SoundManager.getInstance();
        sound.init(this);
        ring = MediaPlayer.create(LevelOne.this, R.raw.background);
        ring.setLooping(true);
        ring.start();
        song = MediaPlayer.create(LevelOne.this, R.raw.song_family);
        correct = MediaPlayer.create(LevelOne.this, R.raw.correct);
        incorrect = MediaPlayer.create(LevelOne.this, R.raw.incorrect);

        double dens = Resources.getSystem().getDisplayMetrics().density;
        screen_width = Resources.getSystem().getDisplayMetrics().widthPixels / dens;
        screen_height = Resources.getSystem().getDisplayMetrics().heightPixels / dens;

        scaleScreen(father);
        scaleScreen(mother);
        scaleScreen(grandfather);
        scaleScreen(grandmother);
        scaleScreen(play);
        scaleScreen(manual);
        scaleScreen(review);
        scaleScreen(brother);
        scaleScreen(sister);
        scaleScreen(family);
        scaleScreen(bee_one);
        scaleScreen(bee_two);
        scaleScreen(bee_three);
        scaleScreen(back);
        scaleScreen(home);
        scaleScreen(board);
        scaleScreen(vocab);

        int text_size = (int) (20 * screen_width / 592);
        vocab.setTextSize(text_size);
//        Log.d("hien", "width: " + screen_width + " height: " +screen_height);

    }

    @Override
    protected void onPause() {
        super.onPause();
        ring.pause();
        song.pause();
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            screenOn = pm.isInteractive();
        } else {
            screenOn = pm.isScreenOn();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (song.isPlaying())
            song.start();
        if (!isPlaying)
            ring.start();
        screenOn = false;
    }

    public void scaleScreen (View view){
        ConstraintLayout.LayoutParams layoutParams = new ConstraintLayout.LayoutParams(view.getLayoutParams());
        int left = (int) (((ConstraintLayout.LayoutParams) view.getLayoutParams()).leftMargin * screen_width/592);
        int top = (int) (((ConstraintLayout.LayoutParams) view.getLayoutParams()).topMargin * screen_height/ 360);
        layoutParams.height = (int) (view.getLayoutParams().height * screen_height/360);
        layoutParams.width = (int) (view.getLayoutParams().width * screen_width/592);
        view.setLayoutParams(layoutParams);
        view.setX(left);
        view.setY(top);
    }

    public void clickOnBack(View view){
        back.startAnimation(animation_small);
        Intent myIntent = new Intent(LevelOne.this, Level.class);
        startActivity(myIntent);
    }

    public void clickOnHome(View view){
        home.startAnimation(animation_small);
        Intent myIntent = new Intent(LevelOne.this, MainActivity.class);
        startActivity(myIntent);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void clickOnDad(View view){
        if (!isReview && !isPlaying){
            sound.playSound(R.raw.father);
            vocab.setText("father");
            father.startAnimation(animation);
        } else {
            if (isPlaying){}
            else if (order == 1) {
                count --;
                correct .start();
                view.setVisibility(View.INVISIBLE);
                checkWin();

            } else {
                incorrect.start();
                live--;
                checkLive();
            }
        }
    }

    public void clickOnMother(View view){
        if (!isReview && !isPlaying){
            sound.playSound(R.raw.mother);
            vocab.setText("mother");
            mother.startAnimation(animation);
        } else {
            if (isPlaying){}
            else if (order == 2) {
                count --;
                correct .start();
                view.setVisibility(View.INVISIBLE);
                checkWin();
            } else {
                incorrect.start();
                live--;
                checkLive();
            }
        }
    }

    public void clickOnGrandma(View view){
        if (!isReview && !isPlaying){
            sound.playSound(R.raw.grandmother);
            vocab.setText("grandmother");
            grandmother.startAnimation(animation);
        } else {
            if (isPlaying){}
            else if (order == 3) {
                count --;
                correct .start();
                view.setVisibility(View.INVISIBLE);
                checkWin();
            } else {
                incorrect.start();
                live--;
                checkLive();
            }
        }
    }

    public void clickOnGrandpa(View view){
        if (!isReview && !isPlaying){
            sound.playSound(R.raw.grandfather);
            vocab.setText("grandfather");
            grandfather.startAnimation(animation);
        } else {
            if (isPlaying){}
            else if (order == 4) {
                count --;
                correct .start();
                view.setVisibility(View.INVISIBLE);
                checkWin();
            } else {
                incorrect.start();
                live--;
                checkLive();
            }
        }
    }

    public void clickOnBrother(View view){
        if (!isReview && !isPlaying){
            sound.playSound(R.raw.brother);
            vocab.setText("brother");
            brother.startAnimation(animation);
        } else {
            if (isPlaying){}
            else if (order == 5) {
                count --;
                correct .start();
                view.setVisibility(View.INVISIBLE);
                checkWin();
            } else {
                incorrect.start();
                live--;
                checkLive();
            }
        }
    }

    public void clickOnSister(View view){
        if (!isReview && !isPlaying){
            sound.playSound(R.raw.sister);
            vocab.setText("sister");
            sister.startAnimation(animation);
        } else {
            if (isPlaying){}
            else if (order == 6) {
                count --;
                correct .start();
                view.setVisibility(View.INVISIBLE);
                checkWin();
            } else {
                incorrect.start();
                live--;
                checkLive();
            }
        }
    }

    public void clickOnFamily(View view){
        if (!isReview && !isPlaying){
            sound.playSound(R.raw.familly);
            vocab.setText("family");
            family.startAnimation(animation);
        } else {
            if (isPlaying){}
            else if (order == 7) {
                count --;
                correct .start();
                view.setVisibility(View.INVISIBLE);
                checkWin();
            } else {
                incorrect.start();
                live--;
                checkLive();
            }
        }
    }

    public void clickOnPlay(View view){
        play.setImageResource(R.drawable.play);
        manual.setImageResource(R.drawable.manual_none);
        review.setImageResource(R.drawable.review_none);
        setVisible();
        ring.pause();
        isPlaying = true;
        song = MediaPlayer.create(LevelOne.this, R.raw.song_family);
        song.start();


        setTime(4600, "mom", mother);
        setTime(5870, "dad", father);
        setTime(6900, "mom", mother);
        setTime(7880, "dad", father);
        setTime(9100, "mom", mother);
        setTime(10200, "dad", father);
        setTime(11300, "mom", mother);
        setTime(12200, "dad", father);
        setTime(13500, "brother", brother);
        setTime(14600, "sister", sister);
        setTime(15700, "brother", brother);
        setTime(16600, "sister", sister);
        setTime(18000, "brother", brother);
        setTime(19000, "sister", sister);
        setTime(20100, "brother", brother);
        setTime(21000, "sister", sister);
        setTime(22400, "grandma", grandmother);
        setTime(23600, "grandpa", grandfather);
        setTime(24700, "grandma", grandmother);
        setTime(25600, "grandpa", grandfather);
        setTime(26900, "grandma", grandmother);
        setTime(28000, "grandpa", grandfather);
        setTime(29100, "grandma", grandmother);
        setTime(30000, "grandpa", grandfather);
        setTime(35800, "family", family);
    }

    public void setTime(int time, String info, ImageView view){
        final String information = info;
        final ImageView this_view = view;
        new CountDownTimer(time, 1000) {
            @Override
            public void onTick(long l) { //loi khi pause
                if (play.getDrawable().getConstantState() == getResources().getDrawable(R.drawable.play_none).getConstantState()){
                    cancel();
                }

                if(screenOn){
                    cancel();
                }
            }
            public void onFinish() {
                vocab.setText(information);
                this_view.startAnimation(animation);
            }
        }.start();
    }

    public void clickOnManual(View view){ //click qua mode khac thi timer chua stop
        play.setImageResource(R.drawable.play_none);
        manual.setImageResource(R.drawable.manual);
        review.setImageResource(R.drawable.review_none);
        setVisible();
        isPlaying = false;
        if (song.isPlaying())
            song.pause();
        if (!ring.isPlaying())
            ring.start();
    }

    public void clickOnReview(View view){
        play.setImageResource(R.drawable.play_none);
        manual.setImageResource(R.drawable.manual_none);
        review.setImageResource(R.drawable.review);
        isReview = true;
        isPlaying = false;
        if (song.isPlaying())
            song.pause();
        if (!ring.isPlaying())
            ring.start();
        random();
    }

    public void clickOnClose(View view){
        if (win.getVisibility() == View.VISIBLE){
            win.setVisibility(View.INVISIBLE);
            close.setVisibility(View.INVISIBLE);
        }

        if (lose.getVisibility() == View.VISIBLE){
            lose.setVisibility(View.INVISIBLE);
            close.setVisibility(View.INVISIBLE);
        }
    }

    public void clickOnWin(View view){
        Intent myIntent = new Intent(LevelOne.this, Level.class);
        startActivity(myIntent);
    }

    public void clickOnLose(View view){
        lose.setVisibility(View.INVISIBLE);
        close.setVisibility(View.INVISIBLE);
    }

    public void setVisible(){
        isReview = false;
        father.setVisibility(father.VISIBLE);
        mother.setVisibility(mother.VISIBLE);
        grandmother.setVisibility(grandmother.VISIBLE);
        grandfather.setVisibility(grandfather.VISIBLE);
        brother.setVisibility(brother.VISIBLE);
        sister.setVisibility(sister.VISIBLE);
        family.setVisibility(family.VISIBLE);
        bee_one.setVisibility(bee_one.VISIBLE);
        bee_two.setVisibility(bee_two.VISIBLE);
        bee_three.setVisibility(bee_three.VISIBLE);

    }

    public void checkLive(){
        switch (live){
            case 1:
                bee_two.setVisibility(bee_two.INVISIBLE);
                break;
            case 2:
                bee_one.setVisibility(bee_one.INVISIBLE);
                break;
            case 3:
                break;
            case 0:
                bee_three.setVisibility(bee_three.INVISIBLE);
                fail();
                break;
        }
    }

    public void fail(){
        sound.playSound(R.raw.lose);
        play.setImageResource(R.drawable.play_none);
        manual.setImageResource(R.drawable.manual);
        review.setImageResource(R.drawable.review_none);
        setVisible();
        count = 7;
        live = 3;
        BASE = "1234567";
        remain = new StringBuilder(BASE);
        bee_one.setVisibility(bee_one.VISIBLE);
        bee_two.setVisibility(bee_two.VISIBLE);
        bee_three.setVisibility(bee_three.VISIBLE);
        lose.setVisibility(View.VISIBLE);
        close.setVisibility(View.VISIBLE);
    }

    public void success (){
        play.setImageResource(R.drawable.play_none);
        manual.setImageResource(R.drawable.manual);
        review.setImageResource(R.drawable.review_none);
        setVisible();
        count = 7;
        live = 3;
        BASE = "1234567";
        remain = new StringBuilder(BASE);
        bee_one.setVisibility(bee_one.VISIBLE);
        bee_two.setVisibility(bee_two.VISIBLE);
        bee_three.setVisibility(bee_three.VISIBLE);
        win.setVisibility(View.VISIBLE);
        close.setVisibility(View.VISIBLE);
    }

    public void checkWin (){
        if (count == 0){
            sound.playSound(R.raw.perfect);
            success();
        } else {
            random();
        }
    }

    public void random(){
        StringBuilder builder = new StringBuilder();

        int character = (int)(Math.random()*BASE.length());
        builder.append(BASE.charAt(character));
        order = Integer.parseInt(builder.toString());
        BASE = remain.deleteCharAt(character).toString();
        switch (order){
            case 1:
                sound.playSound(R.raw.father);
                vocab.setText("father");
                break;
            case 2:
                sound.playSound(R.raw.mother);
                vocab.setText("mother");
                break;
            case 3:
                sound.playSound(R.raw.grandmother);
                vocab.setText("grandmother");
                break;
            case 4:
                sound.playSound(R.raw.grandfather);
                vocab.setText("grandfather");
                break;
            case 5:
                sound.playSound(R.raw.brother);
                vocab.setText("brother");
                break;
            case 6:
                sound.playSound(R.raw.sister);
                vocab.setText("sister");
                break;
            case 7:
                sound.playSound(R.raw.familly);
                vocab.setText("family");
                break;
        }
    }
}
