package com.example.admin.englisharts;

import android.content.Intent;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class Level extends AppCompatActivity {

    private MediaPlayer ring;
    private ImageView level1, level2, level3, home;
    Animation animation, animation_small;

    double screen_width, screen_height;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level);
        ring = MediaPlayer.create(Level.this, R.raw.level_background);
        ring.setLooping(true);
        level1 = (ImageView) findViewById(R.id.level1);
        level2 = (ImageView) findViewById(R.id.level2);
        level3 = (ImageView) findViewById(R.id.level3);
        home = (ImageView) findViewById(R.id.home);
        animation = AnimationUtils.loadAnimation(this.getApplicationContext(), R.anim.myanimation);
        animation_small = AnimationUtils.loadAnimation(this.getApplicationContext(), R.anim.myanimationsmall);

        double dens = Resources.getSystem().getDisplayMetrics().density;
        screen_width = Resources.getSystem().getDisplayMetrics().widthPixels / dens;
        screen_height = Resources.getSystem().getDisplayMetrics().heightPixels / dens;
        scaleScreen(level1);
        scaleScreen(level2);
        scaleScreen(level3);
        scaleScreen(home);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ring.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        ring.start();
    }

    public void scaleScreen (View view){
        ConstraintLayout.LayoutParams layoutParams = new ConstraintLayout.LayoutParams(view.getLayoutParams());
        int left = (int) (((ConstraintLayout.LayoutParams) view.getLayoutParams()).leftMargin * screen_width/592);
        int top = (int) (((ConstraintLayout.LayoutParams) view.getLayoutParams()).topMargin * screen_height/ 360);
        layoutParams.height = (int) (view.getLayoutParams().height * screen_height/360);
        layoutParams.width = (int) (view.getLayoutParams().width * screen_width/592);
        view.setLayoutParams(layoutParams);
        view.setX(left);
        view.setY(top);
    }

    public void clickOnLevel1(View view){
        level1.startAnimation(animation);
        Intent myIntent = new Intent(Level.this, LevelOne.class);
        startActivity(myIntent);
    }

    public void clickOnLevel2(View view){
        level2.startAnimation(animation);
        Intent myIntent = new Intent(Level.this, LevelTwo.class);
        startActivity(myIntent);
    }

    public void clickOnLevel3(View view){
        level3.startAnimation(animation);
        Intent myIntent = new Intent(Level.this, LevelThree.class);
        startActivity(myIntent);
    }

    public void clickOnHome(View view){
        home.startAnimation(animation_small);
        Intent myIntent = new Intent(Level.this, MainActivity.class);
        startActivity(myIntent);
    }
}
